<?php

namespace App\Modules\Users\Providers;

use App\Modules\Users\Repositories\Contracts\UserRepository;
use App\Modules\Users\Repositories\UserRepositoryEloquent;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class UsersModuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mapWebRoutes();
    }

    public function register()
    {
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
    }

    private function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => 'App\Modules\Users\Http\Controllers',
            'prefix' => 'api',
            'as' => 'users.'
        ], function () {
            require app_path('Modules/Users/Http/routes.php');
        });
    }
}
