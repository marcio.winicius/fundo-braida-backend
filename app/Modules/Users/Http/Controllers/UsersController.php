<?php

namespace App\Modules\Users\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Modules\Users\Repositories\Contracts\UserRepository;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    private $usersRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->usersRepository = $userRepository;
    }

    public function index(Request $request)
    {
        dd('teste');
        return $this->usersRepository->paginate($request->get('limit', 15));
    }

    public function show($id)
    {
        return $this->usersRepository->find($id);
    }
}
