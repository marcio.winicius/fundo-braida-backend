<?php

namespace App\Modules\Users\Services;


use App\Modules\Users\Repositories\Contracts\UserRepository;

class UsersService
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
}
