<?php

namespace App\Modules\Users\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface UserRepository extends RepositoryInterface
{

}
