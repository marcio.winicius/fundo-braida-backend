<?php

namespace App\Modules\Users\Repositories;

use App\Modules\Users\Entities\User;
use App\Modules\Users\Presenters\UserPresenter;
use App\Modules\Users\Repositories\Contracts\UserRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    protected $fieldSearchable = [

    ];

    public function model()
    {
        return User::class;
    }

    public function presenter()
    {
        return new UserPresenter();
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
