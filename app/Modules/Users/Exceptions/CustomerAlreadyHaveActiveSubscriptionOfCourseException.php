<?php

namespace App\Modules\Users\Exceptions;

use App\Core\Exceptions\AbstractException;

class CustomerAlreadyHaveActiveSubscriptionOfCourseException extends AbstractException
{
    public function __construct(string $message)
    {
        parent::__construct(['error' => $message]);
    }
}
